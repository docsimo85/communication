import {Injectable} from '@angular/core';

@Injectable()
export class ProductParameterService {

  // parameters bag
  showImage: boolean;
  filterBy: string;

  // config value
  imageWidth: number = 80;
  imageMargin: number = 3;

  constructor() {
  }

}
