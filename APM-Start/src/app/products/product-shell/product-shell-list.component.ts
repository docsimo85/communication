import {Component, OnDestroy, OnInit} from '@angular/core';

import {IProduct} from '../product';
import {ProductService} from '../product.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'pm-product-shell-list',
  templateUrl: './product-shell-list.component.html'
})
export class ProductShellListComponent implements OnInit, OnDestroy {

  pageTitle: string = 'Products';
  errorMessage: string;
  products: IProduct[];
  selectedProduct: IProduct | null;

  sub: Subscription;
  sub2: Subscription;

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.sub = this.productService.selectedProductChanges$.subscribe(
      selected => this.selectedProduct = selected
    );
    this.sub2 = this.productService.getProducts().subscribe(
      (products: IProduct[]) => {
        this.products = products;
      },
      (error: any) => this.errorMessage = <any>error
    );
  }

  onSelected(product: IProduct) {
    this.productService.changeSelectedProduct(product);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }

}
